class Trap extends GraphicObject implements IActivable { ///Piège dans le jeu
  float trapWidth = 76;
  float trapHeight = 76;
  int trapType;
  PVector trapAction; 
  ArrayList<Particle> particles;
  int total = 100;
  boolean activateParticule = true;
  boolean visited = false;
  boolean isActivated = false;
  int i;
  int j;
  int centerValue = 2;
  Trap() ///Constructeur
  {
    particles = new ArrayList<Particle>();
    this.location = new PVector();
    trapType = int(random(2,4));
    for (int i = 0; i < total; i++) {
      particles.add(new Particle(new PVector(400,400)));
    }
  }
 
  Trap(PVector loc) ///Constructeur avec location
  {
    loc.y+= centerValue;
    loc.x+= centerValue;
    particles = new ArrayList<Particle>();
    this.location = loc;
    trapType = int(random(2,4));
  }
  
   public boolean IsColliding(PlayerCharacter pc) ///Regarde si collision avec le joueur
  {
    if((this.location.x <=  pc.location.x + pc.playerWidth-5) && (this.location.x + this.trapWidth > pc.location.x+5)&&(this.location.y <= pc.location.y + pc.playerHeight-5) && (this.location.y + this.trapHeight > pc.location.y+5)){
        for (int i = 0; i < total; i++) {
          particles.add(new Particle(new PVector(this.location.x+trapWidth/2,this.location.y+trapHeight/2)));
        }      
        return true;
    }
     return false;
  }  
 
  void update(float deltaTime) ///Si le piège ne s'est pas déclenché, regarde la collision
  {
     if(isActivated == false)
     {
        if(IsColliding(player))
        {
         activate();
        }
     }
  }
 
  void display() { ///Si le piège n'est pas activé, il se display, sinon il lance les particules
    if(isActivated == false)
     {
       noStroke();
       fill(255,0,200,100);
       if(camera.cameraMoved == true)
       {
         this.location.x -=camera.pos.x;
       }
       pushMatrix();
       rect(this.location.x, this.location.y, trapWidth, trapHeight ); 
       popMatrix();
     }
     else{
          for(Particle p : particles)
       p.run();  
     }
  }
  
  void activate()  ///Applique sa caractéristique de piège spécifique
  {  
   isActivated = true;
   switch(trapType)
     {
       case 1: 
         randomLocation = int(random(1, labyrinth.cellGrid.size()));
         player.location=(trapAction = new PVector(labyrinth.cellGrid.get(randomLocation).getLocation().x, labyrinth.cellGrid.get(randomLocation).getLocation().y));
         break;
       case 2: 
         walkValueX = 1;
         walkValueY = 1;
         time = millis();
         break;
       case 3:  
       int side = int(random(1,5));
       
       switch(side)
       {
         case 1:
           player.applyForce(new PVector(-3,0)); 
         break;
         case 2:
           player.applyForce(new PVector(3,0)); 
         break;
         case 3:
           player.applyForce(new PVector(0,-3));
         break;
         case 4:
           player.applyForce(new PVector(0,3));
         break;
       
         default:
         }
         time = millis();
         rectMode(CORNER); 
         break;
       default: {}
     }
  }
    public boolean getIsActivated(){ ///Retourne s'il est activé
      return isActivated;
    }

     
  String getType(){ ///Retourne le type
     return "Je suis un cell de type <Trap>";
  }
  PVector getLocation(){ return this.location;  }
  
  
  
  
  Cell checkNeigbors(){return null;}
  void setVisited(boolean visited){  }
  void setWalls(int side,boolean change){ } 
  int getI(){ return 0;}
  int getJ(){ return 0; }
  int index( int i, int j){return 0;}
  boolean getVisited(){ return false; }
}
