public class BeginState implements GameState{ ///État de départ au Game
  @Override
  public void next(Game game){
    game.setGameState(new PlayState());
  }
  
  @Override
  public void previous(Game game){ //Retourne à l'état précédent
    println("État de base");
  }
  ///Setup utile pour les composantes de PlayState
  @Override
  public void Setup() ///Setup nécessaire au redémmarage du jeu
  {
    cellFacto = new CellFactory();
    renderer = (PGraphicsJava2D)g;
    camera = new Camera();
    labyrinth = new Labyrinth();
    player = PlayerCharacter.getInstance();
    player.location = new PVector(10,10);
    move = Skeleton_move.IDLE;
    traps = new ArrayList<Cell>();
    creationGoalAndTrap = false;
  }
  
  @Override
  public void Draw(){}
  
  @Override
  public void Update(){}
  
  @Override
  public void Display()
  {}
  
  @Override
  public void Reset()
  {}


}
