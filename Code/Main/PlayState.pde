class PlayState implements GameState{ ///État jouer, le programme est la majorité du temps ici
  
  @Override
  public void next(Game game){ ///Direction prochain état
    game.setGameState(new ReplayState());
  }
  
  @Override
  public void previous(Game game){ ///Direction l'état précédent
    game.setGameState(new BeginState());
  }
  
  @Override
  public void Setup(){ }
  
  @Override
  public void Draw(){  /// Draw des différents objets au momment de jouer
  currentTime = millis();
  deltaTime = currentTime - previousTime;
  previousTime = currentTime;
  Display();
  
  if(keyPressed) {
    if (key == 'r') {
          skeleton.setDead(true);
          replay = true;
      } else if (key == 'w'){
          move = Skeleton_move.UP;  
          player.location.y -= walkValueY; 
      } else if (key == 'a'){
          move = Skeleton_move.LEFT;
          player.location.x -= walkValueX;
      } else if (key == 's'){
          move = Skeleton_move.DOWN;
          player.location.y += walkValueY; 
      } else if (key == 'd'){
          move = Skeleton_move.RIGHT;
          player.location.x += walkValueX;
      } 
}
  camera.cameraMoved = false;
  camera.draw();
  if (millis()>= timeWall+waitWall) { ///Évite que la caméra continue quand le joueur rencontre un mur
     player.wallTouched = false;
     timeWall = millis();
  }
  switch(move) ///On place les différentes séquences de sprite selon le mouvement du joueur
  {
   case UP:
        skeleton.setFrameSequence(0, 8, 0.06, 1);     
        break;
  case LEFT:
        skeleton.setFrameSequence(9, 17, 0.06, 1);
        break;
  case RIGHT:
        skeleton.setFrameSequence(27, 35, 0.06, 1);
        break;
  case DOWN:
        skeleton.setFrameSequence(18, 26, 0.06, 1);
        break;
  case IDLE:
        skeleton.setFrameSequence(18, 18, 0.07, 1);
        break;
  default:
  }
  S4P.drawSprites(); ///Display le sprite
  float elapsedTime = (float) sw.getElapsedTime();
  S4P.updateSprites(elapsedTime); ///Update le sprite
  Update();
  }
  
  @Override
  public void Update(){ ///Update des différents objets au momment de jouer
  player.update(deltaTime);
  labyrinth.update(deltaTime);
  goal.update(deltaTime);
  for (Cell t : traps){ 
    t.update(deltaTime);
    if(t.getIsActivated())
    {
      if(millis() > time+ wait)
      {
        walkValueX = walkValueXOrigin;
        walkValueY = walkValueYOrigin;
        player.velocity.x = 0;
        player.velocity.y = 0;
      }
    }
  }
  skeleton.setXY(player.location.x+player.playerWidth/2, player.location.y+player.playerHeight/2);
  
  }
  
  @Override
  public void Display() ///Display des différents objets au momment de jouer
  {
   background(255);
   labyrinth.display();
 if(!creationGoalAndTrap) /// Doit être fait après le premier display de labyrinth, sinon les cells sont tous à la première case
 {
    randomLocation = int(random(1, labyrinth.cellGrid.size()));
    goal = cellFacto.createWithPos("Goal",new PVector(labyrinth.cellGrid.get(randomLocation).getLocation().x,labyrinth.cellGrid.get(randomLocation).getLocation().y));
  
   
   for (int i = 0; i < NbTraps; i++) {
    randomLocation = int(random(1, labyrinth.cellGrid.size()));
    Cell b = cellFacto.createWithPos("Trap",new PVector(labyrinth.cellGrid.get(randomLocation).getLocation().x,labyrinth.cellGrid.get(randomLocation).getLocation().y));
    traps.add(b);
  }
 creationGoalAndTrap = true;
 }
  for (Cell b : traps) 
    b.display();
    
  goal.display();
  }
  
     @Override
  public void Reset()
  {
  }
}
