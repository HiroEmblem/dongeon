public class Game{  ///Le jeu
  
  private GameState state = new BeginState(); ///L'état du jeu
  
  public void previousState(){ ///Retourne l'état du jeu à l'état précédent
     state.previous(this);
  }
  
  public void nextState(){    ///Retourne l'état du jeu au prochain
    state.next(this);
  }
  
  void setGameState(GameState g){  ///set le state
      this.state = g;
  }
  
  GameState getGameState(){    ///retourne le state
      return state;
  }
}
