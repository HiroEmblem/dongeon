class Camera { ///Classe de la camera
  PVector pos; //Camera's position 
  boolean cameraMoved;

  Camera() { ///Constructeur
    pos = new PVector(0, 0);
    cameraMoved = false;
  }

  void draw() {
    if(player.wallTouched == false)  ///Quand le joueur touche un mur, la caméra doit éviter de continuer son chemin
    {
    if (keyPressed) {
        if (key == 'a') 
        {  
          pos.x = -1; cameraMoved = true;
        }
        else  if (key == 'd') {
          pos.x = 1; cameraMoved = true;
        }
      }
    } 
  }
}
