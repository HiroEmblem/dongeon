import processing.core.*;

public class PlayerCharacter extends PApplet {  ///Joueur
  
  PVector location;
  PVector velocity;
  PVector acceleration;
  private static PlayerCharacter instance = null; ///L'instance du joueur est au départ null
  float playerWidth = 30;
  float playerHeight = 50;
  float mass = 5;
  float previousAction;
  boolean wallTouched = false;
PlayerCharacter () {      ///Contructeur
    location = new PVector();
    velocity = new PVector();
    acceleration = new PVector();
  }
  
  PlayerCharacter(PVector loc) {  ///Contructeur avec location
   this.location = loc;
   velocity = new PVector();
    acceleration = new PVector();
  }
  
  void update(float deltaTime) { ///Update de la vitesse des forces appliquées sur le joueur
    velocity.add(acceleration);
    location.add(velocity);
    acceleration.mult(0);
  }
  
  public static PlayerCharacter getInstance() ///On va chercher l'instance du joueur
    { 
        if (instance == null) ///Si l'instance du joueur n'existe pas, il s'en crée un
            instance = new PlayerCharacter(); 
  
        return instance; 
    } 

  void display() {  ///Il y a un display seulement pour le besoin du debug, car on voit à la place le sprite  
  stroke(0);
  fill(255);
  pushMatrix(); 
   translate(location.x,location.y);
    rect(0, 0, playerWidth, playerHeight ); 
  popMatrix();
  }
 

   void applyForce(PVector force) { ///Forces appliquées sur le joueur
    PVector f = PVector.div(force,mass);
    acceleration.add(f);
  }
}
