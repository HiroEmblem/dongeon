class Labyrinth extends GraphicObject  { ///La labyrinth est construit aléatoirement ici
  float cols;
  float rows;
  int w = 80; ///width
  float addWidth = 240;
  ArrayList<Cell> cellGrid;
  ArrayList<Cell> stack;
  Cell current;
  
  Labyrinth () {      ///Constructeur
    location = new PVector();
    cellGrid = new ArrayList<Cell>();
    stack = new ArrayList<Cell>();
    cols = int((width+addWidth)/w);
    rows = int(height/w);
    PVector cellPosition;
    
    for( int j = 0; j < rows; j++)
    {  
      for(int i = 0; i < cols; i++)
      {
        cellPosition = new PVector(i,j);
        Cell cell = cellFacto.createWithPos("Regular cell", cellPosition);
        cellGrid.add(cell);
      } 
    }
  current = cellGrid.get(0);
 }
  
  Labyrinth (PVector loc) {  ///Constructeur avec location
    this.location = loc;
    cellGrid = new ArrayList<Cell>();
    stack = new ArrayList<Cell>();
    cols = int(width/w);
    rows = int(height/w);
    PVector cellPosition;
    for( int j = 0; j < rows; j++)
    {
      for(int i = 0; i < cols; i++)
      {
        cellPosition = new PVector();
        Cell cell =  cellFacto.createWithPos("Regular cell", cellPosition);
        cellGrid.add(cell);
      } 
    }
  current = cellGrid.get(0);
  }
  
  void update(float deltaTime) { }

  void display() {  ///Display le labyrinth, passe le tableau de cellule
  for(Cell c : cellGrid )
  {
      c.display();  
  }
  current.setVisited(true);
  
  Cell next = current.checkNeigbors();
  if(next != null){
      if(findExit(next) == -1)
      {
        removeWalls(current,next);
      }
      else
      {
        current.setVisited(true);
        stack.add(0,current);
        removeWalls(current,next);
        current = next;
      }
   } else if (stack.size() > 0){
      current = stack.get(0);
      stack.remove(0);
    }
 }
  
  void removeWalls(Cell firstCell, Cell secondCell){  ///Retire les murs des cellules
    int x = firstCell.getI()-secondCell.getI();
    if(x == 1){
      firstCell.setWalls(3,false);
      secondCell.setWalls(1,false);
    }else if(x == -1){
       firstCell.setWalls(1,false);
      secondCell.setWalls(3,false);
    }
    
    int y = firstCell.getJ()-secondCell.getJ();
    if(y == 1){
      firstCell.setWalls(0,false);
      secondCell.setWalls(2,false);
    }else if(y == -1){
      firstCell.setWalls(2,false);
      secondCell.setWalls(0,false);
    }
  }
  
  int findExit(Cell cell){  ///Recherche la fin
  if(cell.getI() < 0 || cell.getJ() < 0 || cell.getI() > cols - 1 || cell.getJ() > rows-1 )
      return -1;
      
  return 1;    
  }
  
  int getW(){  ///Retourne le width 
    return w;
  }
  
  ArrayList<Cell> getCellGrid(){ ///Retourne le array de cell
    return cellGrid;
  }
  
   float getRows(){ ///Retourne les rangées
    return rows;
  }
  
   float getCols(){ ///Retourne les colonnes
    return cols;
  }
}
