class Goal extends GraphicObject implements IActivable {
  float goalWidth = 76;
  float goalHeight = 76;
  float centerValue = 2;
  color c = color(200, 155, 100);
  boolean visited = false;
  boolean isActivated = false;
  int i;
  int j;
  
  Goal()   ///Constructeur de Goal
  {
    this.location = new PVector();
  }
  
  Goal(PVector loc) ///Constructeur de Goal avec location 
  {
    loc.y+= centerValue;
    loc.x+= centerValue;
    this.location = loc;
  }

   public boolean IsColliding(PlayerCharacter pc)  ///Regarde la collision avec le joueur
  {
    if((this.location.x <=  pc.location.x + pc.playerWidth-5) && (this.location.x + this.goalWidth > pc.location.x+5)&&(this.location.y <= pc.location.y + pc.playerHeight-5) && (this.location.y + this.goalHeight > pc.location.y+5)){      
        return true;
      }
        
    return false;
  }  
 
  void update(float deltaTime) ///À chaque update on va voir s'il y a une collision
  {
    if(IsColliding(player))
      {
       activate();
      }
  }
 
  void display() ///Affiche cet objet
  {
     noStroke();
     fill( 0, green(c),0);
     if(camera.cameraMoved == true)
     {
       this.location.x -=camera.pos.x;
     }
     
     pushMatrix();
     rect(this.location.x, this.location.y, goalWidth, goalHeight ); 
     popMatrix();  
  }
   
   void activate() ///Quand le goal s'active, un message gagné apparait
  {
    fill(50);
    textSize(32);
    text("Gagné ", (width/2)-100, height/2); 
  }  
     
   String getType(){ ///Retourne le type 
     return "Je suis un cell de type <Goal>";
   }
   
    public boolean getIsActivated(){ ///Retourne si cet objet est activé
      return isActivated;
    }
     
   PVector getLocation(){ ///Retourne la location
     return location;
   }
   
   
   
   Cell checkNeigbors(){return null;}
   int index( int i, int j){ return 0;} //<>// //<>//
   void setVisited(boolean hasBeenVisited){}
   int getI(){return 0;}
   int getJ(){ return 0;}
   void setWalls(int side,boolean change){  }
   boolean getVisited(){return false;}
}
