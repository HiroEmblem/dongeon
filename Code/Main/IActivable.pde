public interface IActivable extends Cell{ ///Méthode spécifique pour Goal et Trap
  public boolean IsColliding(PlayerCharacter pc);
  public void activate();
  public  boolean getIsActivated();
}
