public class CellFactory implements AbstractFactory<Cell>{ ///Fabrique de cellule
  @Override
  public Cell create(String cellType){ ///Crétation d'un cellule sans position
    if("Regular cell".equalsIgnoreCase(cellType)){
      return new RegularCell();
    }else if ("Goal".equalsIgnoreCase(cellType)){
      return new Goal();
    }else if ("Trap".equalsIgnoreCase(cellType)){
      return new Trap();  
    }
    return null;
  }
  
  @Override 
  public Cell createWithPos(String cellType, PVector position){ ///Création d'une cellule avec position
    if("Regular cell".equalsIgnoreCase(cellType)){
      return new RegularCell(int(position.x),int(position.y));
    }else if ("Goal".equalsIgnoreCase(cellType)){
      return new Goal(position);   
    } else if ("Trap".equalsIgnoreCase(cellType)){
      return new Trap(position);  
    }
    return null;
  }
}
