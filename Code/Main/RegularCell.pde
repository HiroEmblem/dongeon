class RegularCell extends GraphicObject implements Cell{  ///Cell regulier présent dans le labyrinth
  int i;
  int j;
  int pushValue = 3;
  boolean collide = false;
  boolean[] walls = {true,true,true,true};
  boolean visited = false;
  boolean initializeLocation = false;
  ArrayList<Cell> neighbors;
  Cell top;
  Cell right;
  Cell bottom;
  Cell left;
  
  RegularCell(){} ///Constructeur sans location
  
  RegularCell(int i, int j) {   ///Constructeur avec location
    this.location = new PVector();
    neighbors = new ArrayList<Cell>(); 
    this.i = i;
    this.j = j;
  }

  
  void update(float deltaTime) { }

  void display() {  ///Display de la cellule (les différents murs)
  if(initializeLocation == false)
  {
    this.location.x = this.i*labyrinth.getW();
    this.location.y = this.j*labyrinth.getW();
    initializeLocation = true;
  }
    
 if(camera.cameraMoved == true)
  {
     this.location.x -=camera.pos.x;
  }

  stroke(0);
  boolean hitTop = false;
  boolean hitRight = false;
  boolean hitLeft = false;
  boolean hitBottom = false;

  ///Regarde chacun des côtés dd'==
  if(walls[0]) //top
  {
    line(this.location.x  ,this.location.y  ,this.location.x + labyrinth.getW() ,this.location.y);
    hitTop = lineRect(this.location.x,this.location.y,this.location.x + labyrinth.getW(),this.location.y, player.location.x,player.location.y,player.playerWidth,player.playerHeight);
  }
  if(walls[1]) //right
  {
    line(this.location.x +labyrinth.getW(),this.location.y  ,this.location.x + labyrinth.getW() ,this.location.y + labyrinth.getW() ); 
    hitRight = lineRect(this.location.x +labyrinth.getW(),this.location.y,this.location.x + labyrinth.getW(),this.location.y + labyrinth.getW(), player.location.x,player.location.y,player.playerWidth,player.playerHeight);
  }
  if(walls[2]) //bottom
  {
    line(this.location.x,this.location.y + labyrinth.getW() ,this.location.x+ labyrinth.getW() ,this.location.y + labyrinth.getW()  );
     hitBottom = lineRect(this.location.x,this.location.y + labyrinth.getW(),this.location.x+ labyrinth.getW(),this.location.y + labyrinth.getW(), player.location.x,player.location.y,player.playerWidth,player.playerHeight);
  }
  if(walls[3])  //left
  {
    line(this.location.x ,this.location.y + labyrinth.getW(),this.location.x ,this.location.y  ); 
     hitLeft = lineRect(this.location.x,this.location.y + labyrinth.getW(),this.location.x,this.location.y, player.location.x,player.location.y,player.playerWidth,player.playerHeight);
  }

    if(hitTop)
    {
     if(player.location.y + player.playerHeight/2 > this.location.y ){
      player.location.y += pushValue;
     }
    }
    else if (hitRight)
    {
      if(player.location.x + player.playerWidth/2 < this.location.x + labyrinth.getW()){
       player.location.x -= pushValue;
        player.wallTouched = true;
      }
    }
    else if (hitBottom)
    {
      if(player.location.y + player.playerHeight/2 < this.location.y+ labyrinth.getW() ){
       player.location.y -=  pushValue;
      }
    }
    else if (hitLeft)
    {
      if(player.location.x + player.playerWidth/2 > this.location.x){
       player.location.x +=  pushValue;
       player.wallTouched = true;
      }
    }
 
  if(this.visited) ///Si la case est visitée, on met la couleur mauve
    {
      noStroke();
      fill(255,0,255,100);
      rect(this.location.x,this.location.y,labyrinth.getW(),labyrinth.getW());
    }
  }
  
  Cell checkNeigbors()  ///On regarde les voisins de cette cellule [i,j]
  {
    neighbors = new ArrayList<Cell>();
    if(index(i,j - 1) != -1)
     top    = labyrinth.getCellGrid().get(index(i,j - 1));
     else
     top = null;
    
    if(index(i + 1,j) != -1)
     right  = labyrinth.getCellGrid().get(index(i + 1,j));
     else
     right = null;
    
    if(index(i,j + 1) != -1)
     bottom = labyrinth.getCellGrid().get(index(i,j + 1));
     else
     bottom = null;
    
    if(index(i - 1,j) != -1)
     left   = labyrinth.getCellGrid().get(index(i - 1,j));
     else
     left = null;
    
    
    if ((top != null) && (!top.getVisited()))
    neighbors.add(top);

    if ((right != null)&&(!right.getVisited()))
    neighbors.add(right);
    
    if ((bottom != null)&&(!bottom.getVisited()))
    neighbors.add(bottom);
    
    if ((left != null)&&(!left.getVisited()))
    neighbors.add(left);
    
    if(neighbors.size() > 0){
      int r = floor(random(0,neighbors.size()));
      return neighbors.get(r);
    }
    else
      return null;
    
  }
  
  int index( int i, int j)
  {
    if(i < 0 || j < 0 || i > labyrinth.getCols() - 1 || j > labyrinth.getRows()-1 )
      return -1;
    
    return i + j * int(labyrinth.getCols());
  }

  ///Crée les lignes autours d'une cellS
  boolean lineRect(float x1, float y1, float x2, float y2, float rx, float ry, float rw, float rh) { ///Collision ligne/joueur

  // check if the line has hit any of the rectangle's sides
  // uses the Line/Line function below
  boolean left =   lineLine(x1,y1,x2,y2, rx,ry,rx, ry+rh);
  boolean right =  lineLine(x1,y1,x2,y2, rx+rw,ry, rx+rw,ry+rh);
  boolean top =    lineLine(x1,y1,x2,y2, rx,ry, rx+rw,ry);
  boolean bottom = lineLine(x1,y1,x2,y2, rx,ry+rh, rx+rw,ry+rh);

  // if ANY of the above are true, the line
  // has hit the rectangle
  if (left || right || top || bottom) {
    player.wallTouched = true;
    
    return true;
  }
  return false;
}

// LINE/LINE
boolean lineLine(float x1, float y1, float x2, float y2, float x3, float y3, float x4, float y4) { ///On compare deux lignes ensembles(un côté du joueur et un mur de cellule)

  // calculate the direction of the lines
  float uA = ((x4-x3)*(y1-y3) - (y4-y3)*(x1-x3)) / ((y4-y3)*(x2-x1) - (x4-x3)*(y2-y1));
  float uB = ((x2-x1)*(y1-y3) - (y2-y1)*(x1-x3)) / ((y4-y3)*(x2-x1) - (x4-x3)*(y2-y1));

  // if uA and uB are between 0-1, lines are colliding
  if (uA >= 0 && uA <= 1 && uB >= 0 && uB <= 1) {

    return true;
  }
  return false;
}

  void setVisited(boolean hasBeenVisited){ 
    this.visited = hasBeenVisited;
  }

  
   String getType(){ ///Retourne le type de la cellule
     return "Je suis un cell de type <regularCell>";
   }
   
   int getI(){ 
     return i;
   }
   
    int getJ(){
     return j;
   }
  void setWalls(int side,boolean change){ ///On set si cette cellule possède un côté ouvert ou fermé
      walls[side] = change ;
   }
   
   PVector getLocation(){ ///Retourne la location
     return location;
   }
   
   boolean getVisited(){
     return visited;
   }
   
   boolean getIsActivated(){ return false;} 
}
