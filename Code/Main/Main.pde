import sprites.*;
import sprites.maths.*;
import sprites.utils.*;
import processing.awt.PGraphicsJava2D;

int currentTime;
int previousTime;
int deltaTime;
PlayerCharacter player;
Cell goal;
ArrayList<Cell> traps;
float NbTraps = 10;
float walkValueX = 2;
float walkValueY = 2;
float walkValueXOrigin = 2;;float walkValueYOrigin = 2;


float time;
float wait = 2000;
int randomLocation = 0;
PMatrix2D camMat = new PMatrix2D();
Sprite skeleton;
StopWatch sw = new StopWatch();
PGraphicsJava2D renderer;
Labyrinth labyrinth;
boolean creationGoalAndTrap = false;
 int timeWall = 0;
  int waitWall = 400;
enum Skeleton_move {
  UP,
  LEFT,
  RIGHT,
  DOWN,
  IDLE
}
Game game;
Skeleton_move move;
Camera camera;
boolean replay = false;
CellFactory cellFacto;


void setup () { ///Setup nécessaire au Main
  size (960, 880, JAVA2D);
  currentTime = millis();
  previousTime = millis();
  game = new Game();
  renderer = (PGraphicsJava2D)g;
  skeleton = new Sprite(this, "Skeleton_sprite.png", 9, 4, 20);
  game.getGameState().Setup();
  game.getGameState().next(game);
}

void draw () {
  ///On regarde l'état du state
    if(game.getGameState().getClass() == BeginState.class){
       println("Entrer en mode jeu");
   } else if (game.getGameState().getClass() == PlayState.class){
       game.getGameState().Draw();
   }else if (game.getGameState().getClass() == ReplayState.class){
       game.getGameState().Reset();
       println("Redémarrage ");
   }
   
    if(replay){
    game.getGameState().next(game);
    replay = false;
    }
 }

/***
  The calculations should go here
*/
void update() {

}
  
void keyReleased() {
  ///Quand une touche est relâchée, le sprite du Skeleton devient IDLE
  move = Skeleton_move.IDLE;
}
/***
  The rendering should go here
*/
