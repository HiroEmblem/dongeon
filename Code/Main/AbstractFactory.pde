public interface AbstractFactory<T> {  ///Interface d'une factory abstraite
    T create(String cellType);
    T createWithPos(String cellType, PVector position);
}
