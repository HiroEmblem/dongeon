public interface GameState{  ///Interface au GameState
 void next(Game game);
 void previous(Game game);
 void Setup();
 void Draw();
 void Update();
 void Display();
 void Reset();
}
