public interface Cell { ///Cellule de base
  String getType(); 
  void update(float deltaTime);
  void display();
  PVector getLocation();
  Cell checkNeigbors();
  int index( int i, int j);
  void setVisited(boolean visited);
  public boolean getIsActivated();
  boolean getVisited();
  int  getI();
  int getJ();
  void setWalls(int side,boolean change); 
}
