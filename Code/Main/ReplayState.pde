public class ReplayState implements GameState{ ///État de redémmarage du jeu
     @Override
  public void next(Game game){
   println("État final");
  }
  
  @Override
  public void previous(Game game){
    game.setGameState(new PlayState());
  }  
  
  @Override
  public void Setup(){}
  
  @Override
  public void Draw(){}
  
  @Override
  public void Update(){}
  
  @Override
  public void Display(){}
  
  @Override
  public void Reset(){ ///Détruit le skeleton et repars le setup
    skeleton.setDead(true); 
    setup();
  }
}
