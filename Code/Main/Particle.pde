class Particle extends GraphicObject { 
  PVector location;
  PVector velocity;
  float timeOnScreen;
  float timeOnScreenValue = 100;
  
  Particle() ///Constructeur
  {
     location = new PVector();
     velocity = new PVector();
     acceleration = new PVector();
     timeOnScreen = timeOnScreenValue;
  }
  
   Particle(PVector location) {    ///Constructeur avec location
    acceleration = new PVector(0,0.05);
    velocity = new PVector(random(-1,1),random(-2,0));
    this.location = location;
    timeOnScreen = timeOnScreenValue;
  }
  
  void run() ///Active la particule
  {
    update(deltaTime);
    display();
  }
  
  void update(float deltaTime) ///Une fois la particule lancée, la vitesse de la particule augmente
  {
    velocity.add(acceleration);
    location.add(velocity);
    timeOnScreen -= 2.0;
  }
 
  void display() { ///Display la particule
    
    stroke(0,timeOnScreen);
     if(camera.cameraMoved == true)
     {
       this.location.x -=camera.pos.x;
     }
    fill(255,200,200,timeOnScreen);
    rect(location.x,location.y,8,8);
  }
  
  ///Si le temps à l'écran de la particule est < 0, il disparait
   boolean isDead() {
    if (timeOnScreen < 0.0) {
      return true;
    } else {
      return false;
    }
  }
     
  void applyForce(PVector f) { ///Applique des forces à la particule
      applyForce(f);
  }
}
