var interface_main_1_1_game_state =
[
    [ "Display", "interface_main_1_1_game_state.html#a7d5d3c5c69813ded05183505f411cb08", null ],
    [ "Draw", "interface_main_1_1_game_state.html#a5050ec9373291bc2c98934ab15eb2f75", null ],
    [ "next", "interface_main_1_1_game_state.html#a902dec202fc90e66fad2641787d593e2", null ],
    [ "previous", "interface_main_1_1_game_state.html#abe2c489e9b5f73f7affb0dcdff18958b", null ],
    [ "Reset", "interface_main_1_1_game_state.html#a6880ad14b70084c67da33124c2517fad", null ],
    [ "Setup", "interface_main_1_1_game_state.html#a7916a7e6a0d0834f027268e09f9e8423", null ],
    [ "Update", "interface_main_1_1_game_state.html#a4a8a0eb137b0f6441fb9af20183992f1", null ]
];