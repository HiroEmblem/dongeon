var searchData=
[
  ['nbtraps_70',['NbTraps',['../class_main.html#ae9d9a6731b3db4169428652d99108e4d',1,'Main']]],
  ['neighbors_71',['neighbors',['../class_main_1_1_regular_cell.html#a4ba3788bfe4c46fa95a007b4027ef35a',1,'Main::RegularCell']]],
  ['next_72',['next',['../class_main_1_1_begin_state.html#a4c0cd7da058b8d84a5d522f470357654',1,'Main.BeginState.next()'],['../interface_main_1_1_game_state.html#a902dec202fc90e66fad2641787d593e2',1,'Main.GameState.next()'],['../class_main_1_1_play_state.html#af7d4a79e55d3cdccd2dd36c8374afb1f',1,'Main.PlayState.next()'],['../class_main_1_1_replay_state.html#a851ccd72e60e2b22882c4badea28f043',1,'Main.ReplayState.next()']]],
  ['nextstate_73',['nextState',['../class_main_1_1_game.html#a4aeccea565a0edf21136f6fe862e1cc4',1,'Main::Game']]]
];
