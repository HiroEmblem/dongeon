var searchData=
[
  ['index_179',['index',['../interface_main_1_1_cell.html#ae76991af9b5c37af3d904a50ed1d4e14',1,'Main.Cell.index()'],['../class_main_1_1_goal.html#a0f0e5370a747afcbee1d96be9cc9d719',1,'Main.Goal.index()'],['../class_main_1_1_regular_cell.html#a20a0d0b4f425b8b61b39e49a6c594fc0',1,'Main.RegularCell.index()'],['../class_main_1_1_trap.html#a759a6f3ce18d1c88f39ecb35fbe02ba5',1,'Main.Trap.index()']]],
  ['iscolliding_180',['IsColliding',['../class_main_1_1_goal.html#abdaf41876a9c1917efd9d08d3668f950',1,'Main.Goal.IsColliding()'],['../interface_main_1_1_i_activable.html#a969a089626866e52ca318383c7870d9d',1,'Main.IActivable.IsColliding()'],['../class_main_1_1_trap.html#a32967b42c8ad74df2655d9a51f166ead',1,'Main.Trap.IsColliding()']]],
  ['isdead_181',['isDead',['../class_main_1_1_particle.html#a21b537fcfa3a893c5bb5a49393990597',1,'Main::Particle']]]
];
